@foreach($tasks as $task)
    <tr>
        <th scope="row"> {{$task->id}} </th>
        <td> {{$task->name}} </td>
        <td> {{$task->content}} </td>
        <td>
            <a href="{{route('tasks.edit', $task->id)}}"><button type="submit" class="btn btn-success">Edit</button></a>
            <form action="{{ route('tasks.destroy', $task->id) }}" method="POST">
                @csrf
                @method('delete')
                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
        </td>
    </tr>
@endforeach