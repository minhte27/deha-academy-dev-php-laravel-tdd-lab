<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\TaskController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('tasks/search', [TaskController::class, 'search'])->name('tasks.search');
Route::get('tasks/create', [TaskController::class, 'create'])->name('tasks.create')->middleware('auth');
Route::get('tasks', [TaskController::class , 'index'])->name('tasks.index');
Route::post('tasks', [TaskController::class, 'store'])->name('tasks.store')->middleware('auth');
Route::get('tasks/{id}', [TaskController::class, 'show'])->name('tasks.show');
Route::get('tasks/edit/{id}', [TaskController::class, 'edit'])->name('tasks.edit')->middleware('auth');
Route::put('tasks/{id}', [TaskController::class, 'update'])->name('tasks.update')->middleware('auth');
Route::delete('tasks/{id}', [TaskController::class, 'destroy'])->name('tasks.destroy')->middleware('auth');


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
