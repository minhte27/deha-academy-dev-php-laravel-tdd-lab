<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateTaskTest extends TestCase
{
    public function createTaskRoute()
    {
        return route('tasks.store');
    }

    public function getCreateTaskViewRoute()
    {
        return route('tasks.create');
    }

    /** @test */
    public function authenticate_user_can_new_task(): void
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->make()->toArray();
        $response = $this->post($this->createTaskRoute(), $task);

        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('tasks', $task);
        $response->assertRedirect(route('tasks.index'));
    }

    /** @test */
    public function unauthenticate_user_can_new_task(): void
    {
        $task = Task::factory()->make()->toArray();
        $response = $this->post($this->createTaskRoute(), $task);

        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticate_user_can_not_create_task_if_name_field_id_null(): void
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->make(['name' => null])->toArray();
        $response = $this->post($this->createTaskRoute(), $task);

        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticate_user_can_view_create_task_form(): void
    {
        $this->actingAs(User::factory()->create());
        $response = $this->get($this->getCreateTaskViewRoute());

        $response->assertViewIs('tasks.create');
    }

    /** @test */
    public function authenticate_user_can_see_name_required_text_if_validate_error(): void
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->make(['name' => null])->toArray();
        $response = $this->from($this->getCreateTaskViewRoute())->post($this->createTaskRoute(), $task);

        $response->assertRedirect($this->getCreateTaskViewRoute());
    }

    /** @test */
    public function authenticate_user_can_see_content_required_text_if_validate_error(): void
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->make(['content' => null])->toArray();
        $response = $this->from($this->getCreateTaskViewRoute())->post($this->createTaskRoute(), $task);

        $response->assertRedirect($this->getCreateTaskViewRoute());
    }

    /** @test */
    public function unauthenticate_user_can_not_see_create_task_form_view(): void
    {
        $response = $this->get($this->getCreateTaskViewRoute());

        $response->assertRedirect('/login');
    }
}
